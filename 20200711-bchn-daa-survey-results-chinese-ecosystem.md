BCHN Survey of Chinese Ecosystem about DAA Change in November
=============================================================

Bitcoin Cash Node (BCHN) Representative Tracy Chen consulted with the
Chinese Bitcoin Cash community (big mining pools, exchanges, key persons
and holders) on the question of whether they support a change of
Difficulty Adjustment Algorithm (DAA) in November.

The question was "Do you think DAA should be resolved in the November upgrade?"
We stress that this was an opinion poll and not a binding vote.

The results were as follows:

Mining pools
------------

3 out of 5 in agreement to modify the DAA. The other 2 were neutral.

![Major mining pool opinion on November DAA change](img/20200711-bchn-daa-survey-results-chinese-ecosystem-chart-1.png)

(Mining pools consulted: Antpool, ViaBTC, BTC.top, BTC.com, Huobi pool)

Exchanges
---------

3 out of 4 in agreement to modify DAA, 1 neutral.

![Major exchange opinion on November DAA change](img/20200711-bchn-daa-survey-results-chinese-ecosystem-chart-2.png)

(Exchanges consulted: Huobi, Binance, Gate.io, CoinEX)

Key persons
-----------

1 out of 3 in agreement to modify, 1 neutral, 1 no reply.

![Key person opinion on November DAA change](img/20200711-bchn-daa-survey-results-chinese-ecosystem-chart-3.png)

BCH holders
-----------

29 out of 30 respondents (96.7%) were in agreement to modify for November.

The respondents were from "100 bch club" in Chinese BCH community.

![BCH holders opinion on November DAA change](img/20200711-bchn-daa-survey-results-chinese-ecosystem-chart-4.png)


Let us know your opinion
------------------------

BCHN invites other Bitcoin Cash pools, exchanges and stakeholders to voice their
opinion on this matter.

If you are a BCH stakeholder or someone who is concerned about the DAA or
development of BCH and you have not voiced your opinion yet, you can contact
Tracy on WeChat, the BCHN Telegram or on BCHN Slack as follows:

- TracyC on [Bitcoin Cash Node WeChat](https://weixin.qq.com/g/AQYAAJAOatfv0Pe7UnstYGWU9tybJCvmP05-KfUhUKCFdzWTZnXdhHTl1tBkzo81)
- BCHN Telegram: @bchnodetracy
- BCHN Slack: @Tracy Chen

